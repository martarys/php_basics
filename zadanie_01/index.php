<?php


$title = "Nasza pierwsza strona";

$header = "Kalkulator";

$x = isset($_POST['x'])? (float)$_POST['x']: 0.0;
$y = isset($_POST['y'])? (float)$_POST['y']: 0.0;

$operator = isset($_POST['operator'])? $_POST['operator']: 0;

if(
    ($x || $x === 0.0) &&
    ($y || $y === 0.0) &&
    is_numeric($x) &&
    is_numeric($y)
    ){
        switch ($operator){
            case '1': $wynik=$x+$y;break;
            case '2': $wynik=$x-$y;break;
            case '3': $wynik=$x*$y;break;
            case '4': $wynik=$x/$y;break;
            default: $wynik=0.0;            
        }
}
include_once './html/home.html';