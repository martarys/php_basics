<?php

namespace Vehicles;

class Car{
  public $color;
  public $doors;
  /**
   * @var Wheel[]
   */
  public $wheels = [];
  public $running = false;
  public $broken = false;
  
  public function addWheel(Wheel $wheel){
      $wheel->setCar($this);
      $this->wheels[] = $wheel;
  }
  
  public function getBurstWheels():array{
      $wheels = [];
      foreach ($this->wheels as $wheel){
          if($wheel->isBurst()){
              $wheels[] = $wheel;
          }
      }
      return $wheels;
  }
 
  
  public function broken(){
      $this->broken = true;
  }
  
}
