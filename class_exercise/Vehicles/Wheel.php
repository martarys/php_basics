<?php
namespace Vehicles;

class Wheel{
    /**
     * @var int
     */
    public $height;
    /**
     * @var int
     */
    public $size;
    /**
     * @var Car
     */
    public $car;
    /**
     * *@var bool
     */
    public $burst = false;
    
    public function setCar(Car $car){
        
        $this->car = $car;
    }
    
    public function isBurst():bool {
        return $this->burst;
    }
    
    public function burst(){
        if (is_null($this->car)) {
            throw new \InvalidArgumentException("Car has not been set");
        }
        $this->burst = true;
        $this->car->broken();
        
    }
}