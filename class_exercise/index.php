<?php


spl_autoload_register(
    function (string $name){
        $name = __DIR__ . '/' . str_replace('\\', '/', $name);
        require_once $name . '.php';
    }
);

use Vehicles\Wheel;

try {
    $car = new \Vehicles\Car(); 
    $car->color = 'red';
    $car->doors = '5';
    
    $wheel = new Wheel();
    $wheel->size = 17;
    $wheel->height = 70;
    $wheel_2 = new Wheel();
    $wheel_2->size = 50;
    $wheel_2->height = 70;
    
    $wheel->burst();
    
    $car->addWheel($wheel);
    $car->addWheel($wheel);
    $car->addWheel($wheel_2);
    $car->addWheel(clone($wheel_2));
    
    var_dump($car->getBurstWheels());
    
} Catch(InvalidArgumentException $e) {
    var_dump($e);
}


