<?php

function add(float $a,float $b):float{
  return $a + $b;
}

function deduct(float $a, float $b):float{
    return $a - $b;
}

function multiply(float $a, float $b):float{
    return $a * $b;
}

function divide(float $a, float $b){
    if($b === 0.0){
      return "Nie można dzielić przez zero";
    }

    return $a / $b;
}


echo "dodawanie: " . add(3.1,5.2) . "\n";
echo "odejmowanie: " . deduct(5.3,3.3) . "\n";
echo "mnożenie: " . multiply(5.3,3.3) . "\n";
echo "dzielenie: " . divide(5.3,0) . "\n";
echo "dzielenie: " . divide(5.3,2) . "\n";
